//
// Copyright (c) 2013 Daniel Iñigo Baños
// 
// Permission is hereby granted, free of charge, to any
// person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the
// Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice
// shall be included in all copies or substantial portions of
// the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
// KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
// OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
// SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

/**
 * @file NoFree.cc
 * @author Daniel Iñigo
 */

#include "NoFree.h"
#include "ScribeMessage_m.h"
#include "NoFree_m.h"
#include "ReputationRequest_m.h"
#include <limits.h>
#include <GlobalStatistics.h>

Define_Module(NoFree);

using namespace std;

NoFree::NoFree()
{
    currentRegionX = currentRegionY = INT_MIN;
    currentRegionID = OverlayKey::UNSPECIFIED_KEY;
    reputationTimer = new cMessage("reputationTiemout");
}

NoFree::~NoFree()
{
    cancelAndDelete(reputationTimer);
}

/**
 * Mira el momento en que se cerró la simulación y, si es demasiado pequeño no
 * cuenta como simulación (para no machacar las estadísticas previas).
 */
void NoFree::finishApp()
{
    simtime_t time = globalStatistics->calcMeasuredLifetime(creationTime);
    if (time < GlobalStatistics::MIN_MEASURED) return;

    globalStatistics->addStdDev("NoFree: Lost or too long delayed MoveLists/s",
                                lostMovementLists / time);
    globalStatistics->addStdDev("NoFree: Received valid MoveLists/s",
                                receivedMovementLists / time);
}

void NoFree::initializeApp(int stage)
{
    // Si ya está inicializada sale inmediatamente.
    if (stage != (numInitStages()-1))   return;

    // Obtiene los parámetros declarados en el archivo de topología
    reputationTimeout = par("reputationTimeout");   // tiempo hasta que haya que pedir de nuevo la reputación de un nodo.
    maxMoveDelay = par("maxRRDelay");               // maximo delay al pedir una reputación, despues se deja de esperar.
    AOIWidth = par("AOIWidth");
}


bool NoFree::handleRpcCall( BaseCallMessage* msg )
{
      return false;
}

void NoFree::handleRpcResponse( BaseResponseMessage* msg,
                                cPolymorphic* context,
                                int rpcId, simtime_t rtt )
{
    return false;
}

void NoFree::handleTimerEvent( cMessage *msg )
{
    // Si el mensaje que se recibe es un reputationTimer es que uno de los nodos
    // No está actualizado. Vemos cual (o cuales) son.
    if( msg == reputationTimer ) {

        // Crea un iterador para recorrer la lista de pares
        map<NodeHandle, PeerStatistics>::iterator it;
        // Crea una lista de pares que serán eliminados.
        list<NodeHandle> erasePlayers;
        // Comprueba que la lista de pares a borrar esté vacía y la arregla.
        for( it = peerMap.begin(); it != peerMap.end(); ++it ) {
            if( it->second.update == false ) {
                erasePlayers.push_back( it->first );
            }
            it->second.update = false;
        }

        // Recorre la lista de pares a eliminar.
        for( list<NodeHandle>::iterator it = erasePlayers.begin(); it != erasePlayers.end(); ++it) {
            // Eliminar los pares de los que no se reciben actualizaciones
            GameAPIListMessage *scMsg = new GameAPIListMessage("NEIGHBOR_UPDATE");
            scMsg->setCommand(NEIGHBOR_UPDATE);
            scMsg->setRemoveNeighborArraySize(1);
            scMsg->setRemoveNeighbor(0, *it);
            send(scMsg, "to_upperTier");

            playerMap.erase( *it );
        }
        erasePlayers.clear();

        scheduleAt( simTime() + playerTimeout, msg );
    }
}

void NoFree::handleLowerMessage( cMessage *msg )
{
    if (ALMMulticastMessage* mcastMsg =
            dynamic_cast<ALMMulticastMessage*>(msg) ){

        cMessage* innerMsg = mcastMsg->decapsulate();
        NoFreeReputationMessage* moveMsg = NULL;
        if( innerMsg ) {
                moveMsg = dynamic_cast<NoFreeReputationMessage*>(innerMsg);
        }
        if( moveMsg ) {
            handleReputation( moveMsg );
        }
        delete innerMsg;
        delete mcastMsg;
    }
}

void NoFree::handleFileRequest( cMessage *msg )
{



    //TODO: Obtener el remitente de la peticion
    //TODO: Comprobar que tenermos reputacion de usuario
    //TODO: Comprobar con un if que el remitente tiene good ratio
    //TODO: Añadir estadistica de peticion aceptada/denegada



    // process only ready messages from the tier below
    if( getThisCompType() - msg->getComp() == 1 ){
        if( msg->getReady() ) {
            msg->setComp(getThisCompType());
            send(msg, "to_upperTier");
            // also send AOI size to SimpleGameClient
            GameAPIResizeAOIMessage* gameMsg = new GameAPIResizeAOIMessage("RESIZE_AOI");
            gameMsg->setCommand(RESIZE_AOI);
            gameMsg->setAOIsize(AOIWidth);
            send(gameMsg, "to_upperTier");

            if( playerTimeout > 0 ) {
                cancelEvent( playerTimer );
                scheduleAt( simTime() + playerTimeout, playerTimer );
            }
        }
    } else {
        delete msg;
    }
}

void NoFree::handleUpperMessage( cMessage *msg )
{
    if( GameAPIPositionMessage* moveMsg =
        dynamic_cast<GameAPIPositionMessage*>(msg) ) {
        handleMove( moveMsg );
        delete msg;
    }
}

void NoFree::handleReputationRequest( cMessage * msg){

    ReputationRequest * rrmsg = check_and_cast<ReputationRequest *>(msg);
    int remitenteId = rrmsg->getRemitente();

}

void NoFree::handleMove( GameAPIPositionMessage * msg )
{
    if( (int) (msg->getPosition().x/regionSize) != currentRegionX ||
            (int) (msg->getPosition().y/regionSize) != currentRegionY ) {
        // New position is outside of current region; change currentRegion

        // get region ID
        currentRegionX = (int) (msg->getPosition().x/regionSize);
        currentRegionY = (int) (msg->getPosition().y/regionSize);
        std::stringstream regionstr;
        regionstr << currentRegionX << ":" << currentRegionY;
        OverlayKey region = OverlayKey::sha1( BinaryValue(regionstr.str() ));
        currentRegionID = region;
    }

    set<OverlayKey> expectedRegions;
    set<OverlayKey> allowedRegions;
    int minX = (int) ((msg->getPosition().x - AOIWidth)/regionSize);
    if( minX < 0 ) minX = 0;
    int maxX = (int) ((msg->getPosition().x + AOIWidth)/regionSize);
    if( maxX >= numSubspaces ) maxX = numSubspaces -1;
    int minY = (int) ((msg->getPosition().y - AOIWidth)/regionSize);
    if( minY < 0 ) minY = 0;
    int maxY = (int) ((msg->getPosition().y + AOIWidth)/regionSize);
    if( maxY >= numSubspaces ) maxY = numSubspaces -1;

    int minUnsubX = (int) ((msg->getPosition().x - 1.5*AOIWidth)/regionSize);
    if( minUnsubX < 0 ) minUnsubX = 0;
    int maxUnsubX = (int) ((msg->getPosition().x + 1.5*AOIWidth)/regionSize);
    if( maxUnsubX >= numSubspaces ) maxUnsubX = numSubspaces -1;
    int minUnsubY = (int) ((msg->getPosition().y - 1.5*AOIWidth)/regionSize);
    if( minUnsubY < 0 ) minUnsubY = 0;
    int maxUnsubY = (int) ((msg->getPosition().y + 1.5+AOIWidth)/regionSize);
    if( maxUnsubY >= numSubspaces ) maxUnsubY = numSubspaces -1;

    for( int x = minUnsubX; x <= maxUnsubX; ++x ){
        for( int y = minUnsubY; y <= maxUnsubY; ++y ){
            std::stringstream regionstr;
            regionstr << x << ":" << y;
            if( x >= minX && x <=maxX && y >= minY && y <= maxY ){
                expectedRegions.insert( OverlayKey::sha1( BinaryValue(regionstr.str() )));
            }
            allowedRegions.insert( OverlayKey::sha1( BinaryValue(regionstr.str() )));
        }
    }

    set<OverlayKey>::iterator subIt = subscribedRegions.begin();
    while( subIt != subscribedRegions.end() ){

        expectedRegions.erase( *subIt );

        // unsubscribe region if to far away
        if( allowedRegions.find( *subIt ) == allowedRegions.end() ){
            // Inform other players about region leave
            NoFreeReputationMessage* moveMsg = new NoFreeReputationMessage("MOVE/LEAVE_REGION");
            moveMsg->setSrc( overlay->getThisNode() );
            moveMsg->setPosX( msg->getPosition().x );
            moveMsg->setPosY( msg->getPosition().y );
            moveMsg->setTimestamp( simTime() );
            moveMsg->setLeaveRegion( true );
            ALMMulticastMessage* mcastMsg = new ALMMulticastMessage("MOVE/LEAVE_REGION");
            mcastMsg->setGroupId(*subIt);
            mcastMsg->encapsulate( moveMsg );

            send(mcastMsg, "to_lowerTier");

             // leave old region's multicastGroup
            ALMLeaveMessage* leaveMsg = new ALMLeaveMessage("LEAVE_REGION_GROUP");
            leaveMsg->setGroupId(*subIt);
            send(leaveMsg, "to_lowerTier");

           // Erase subspace from subscribedList and increase iterator
            subscribedRegions.erase( subIt++ );
        } else {
            ++subIt;
        }
    }

    // if any "near" region is not yet subscribed, subscribe
    for( set<OverlayKey>::iterator regionIt = expectedRegions.begin(); regionIt != expectedRegions.end(); ++regionIt ){
        // join region's multicast group
        ALMSubscribeMessage* subMsg = new ALMSubscribeMessage;
        subMsg->setGroupId(*regionIt);
        send( subMsg, "to_lowerTier" );

        subscribedRegions.insert( *regionIt );
    }

    // publish movement
    NoFreeReputationMessage* moveMsg = new NoFreeReputationMessage("MOVE");
    moveMsg->setSrc( overlay->getThisNode() );
    moveMsg->setPosX( msg->getPosition().x );
    moveMsg->setPosY( msg->getPosition().y );
    moveMsg->setTimestamp( simTime() );
    moveMsg->setBitLength( SIMMUD_MOVE_L( moveMsg ));
    ALMMulticastMessage* mcastMsg = new ALMMulticastMessage("MOVE");
    mcastMsg->setGroupId(currentRegionID);
    mcastMsg->encapsulate( moveMsg );

    send(mcastMsg, "to_lowerTier");
}

//recibe un sms de reputacion y actualiza la tabla de reputaciones de vecinos
void NoFree::handleReputation( NoFreeReputationMessage* msg )
{
    GameAPIListMessage *scMsg = new GameAPIListMessage("NEIGHBOR_UPDATE"); //crear un nuevo mensaje de reputacion que vamos a reenviar a los vecinos
    scMsg->setCommand(NEIGHBOR_UPDATE);

    NodeHandle& src = msg->getSrc();
    RECORD_STATS(
            if( msg->getTimestamp() < simTime() - maxMoveDelay ){
                ++lostMovementLists;
                globalStatistics->addStdDev("NoFree: LOST MOVE Delay",
                    SIMTIME_DBL(simTime() - msg->getTimestamp()) );
            } else {
                ++receivedMovementLists;
            }

            if( src != overlay->getThisNode() ){
                globalStatistics->addStdDev("NoFree: MOVE Delay",
                    SIMTIME_DBL(simTime() - msg->getTimestamp()) );
            }
            );

    // TODO:   cambiar este if por uno que compruebe que se cumple el requisito
    //         para servir archivos (>80%)
    if( msg->getLeaveRegion() ) {
        // Player leaves region
        scMsg->setRemoveNeighborArraySize(1);
        scMsg->setRemoveNeighbor(0, src);
        playerMap.erase( src );

    // TODO:    cambiar el else por qué hacer cuando se rechaza.
    } else {
        PlayerInfo player;
        player.pos = Vector2D( msg->getPosX(), msg->getPosY() );
        player.update = true;

        // pair< map<NodeHandle, PlayerInfo>::iterator, bool> inserter =
        playerMap.insert( make_pair(src, player) );
        scMsg->setAddNeighborArraySize(1);
        scMsg->setNeighborPositionArraySize(1);
        scMsg->setAddNeighbor(0, src);
        scMsg->setNeighborPosition(0, Vector2D(msg->getPosX(), msg->getPosY()) );
    }
    send(scMsg, "to_upperTier");

}

