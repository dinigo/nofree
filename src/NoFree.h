//
// Copyright (c) 2013 Daniel Iñigo Baños
// 
// Permission is hereby granted, free of charge, to any
// person obtaining a copy of this software and associated
// documentation files (the "Software"), to deal in the
// Software without restriction, including without limitation
// the rights to use, copy, modify, merge, publish,
// distribute, sublicense, and/or sell copies of the
// Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:
// 
// The above copyright notice and this permission notice
// shall be included in all copies or substantial portions of
// the Software.
// 
// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY
// KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE
// WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR
// PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS
// OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR
// OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR
// OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
// SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

/**
 * @file NoFree.h
 * @author Daniel Iñigo
 */


#ifndef __NOFREE_H_
#define __NOFREE_H_

#include "BaseApp.h"
#include "NoFree_m.h"
#include "NoFree.h"
#include "ReputationRequest_m.h"

class NoFree : public BaseApp
{
private:
    // TODO: aquí se declaran las variables que hacen falta para la negociación
    int reputationTimeout;      // Tiempo hasta que haya que volve a pedir la Reputación
    cMessage* reputationTimer;  // Timer que cuenta hasta que se agora el reputationTimeout

    OverlayKey currentRegionID;
    std::set<OverlayKey> subscribedRegions;

    int AOIWidth;

    int receivedMovementLists;
    int lostMovementLists;
    simtime_t maxRRDelay;       // Timeout de la petición de reputacion

    /**
     * Almacena las estadísticas de un nodo para luego saber si servirle o no
     */
    struct PeerStatistics {
        bool update;
        int acceptedRequest;
        int totalRequests;
    };

    std::map <NodeHandle, PeerStatistics> peerMap;

public:
    NoFree( );
    ~NoFree( );

    /**
     * Inicialización de la aplicación
     */
    virtual void initializeApp( int stage );

    /**
     * Evento periódico. Se puede dar un tiempo de vida a la reputación para
     * obligar a actulizarla tras ese tiempo (encolar este evento cada
     * "ReputationTimeOut", por ejemplo) peticion de reputacion por inundacion
     */
    virtual void handleTimerEvent( cMessage *msg );

    /**
     * Recibe un mensaje tipo `NoFreeReputationMessage` se lo pasa a
     * handleReputation.
     */
    virtual void handleLowerMessage( cMessage *msg );

    /**
     * TODO: reutilizar para implementar la función de servir archivos.
     */
    virtual void handleFileRequest( CompReadyMessage *msg );

    /**
     * TODO: Implementar funcion para contestar con la reputacion.
     */
    virtual void handleReputationRequest( cMessage *msg );

    /**
     * TODO: BUSCAR INFORMACIÓN DE ESTA FUNCIÓN.
     */
    virtual bool handleRpcCall( BaseCallMessage* msg );

    /**
     * TODO: BUSCAR INFORMACIÓN DE ESTA FUNCIÓN.
     */
    virtual void handleRpcResponse( BaseResponseMessage* msg,
            cPolymorphic* context,
            int rpcId, simtime_t rtt );

    /**
     * Recolecta los datos estadísticos si se ha simulado suficiente tiempo.
     */
    virtual void finishApp( );

protected:

    /**
     * Maneja la reputación de otros en una tabla o algo similar. Manda
     * actualizaciones a los demás.
     */
    void handleReputation( NoFreeReputationMessage* msg );
};

#endif
